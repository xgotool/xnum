package xnum

import "encoding/json"

// 移除数组中的元素
//
//	s	待移除的变量
//	arr	待移除变量的数组
func RemoveFloat64Array(s float64, arr []float64) []float64 {
	for i := 0; i < len(arr); i++ {
		if IsEqualFloat(s, arr[i]) {
			return append(arr[:i], arr[i+1:]...)
		}
	}
	return arr
}

// 移除数组中的元素
//
//	s	待移除的变量
//	arr	待移除变量的数组
func RemoveJsonNumberArray(s json.Number, arr []json.Number) []json.Number {
	for i := 0; i < len(arr); i++ {
		if IsEqualFloat(s, arr[i]) {
			return append(arr[:i], arr[i+1:]...)
		}
	}
	return arr
}

// 移除数组中的元素
//
//	s	待移除的变量
//	arr	待移除变量的数组
func RemoveFloat32Array(s float32, arr []float32) []float32 {
	for i := 0; i < len(arr); i++ {
		if IsEqualFloat(s, arr[i]) {
			return append(arr[:i], arr[i+1:]...)
		}
	}
	return arr
}

// 移除数组中的元素
//
//	s	待移除的变量
//	arr	待移除变量的数组
func RemoveUint8Array(s uint8, arr []uint8) []uint8 {
	for i := 0; i < len(arr); i++ {
		if s == arr[i] {
			return append(arr[:i], arr[i+1:]...)
		}
	}
	return arr
}

// 移除数组中的元素
//
//	s       待移除的变量
//	arr     待移除变量的数组
func RemoveInt8Array(s int8, arr []int8) []int8 {
	for i := 0; i < len(arr); i++ {
		if s == arr[i] {
			return append(arr[:i], arr[i+1:]...)
		}
	}
	return arr
}

// 移除数组中的元素
//
//	s       待移除的变量
//	arr     待移除变量的数组
func RemoveUint16Array(s uint16, arr []uint16) []uint16 {
	for i := 0; i < len(arr); i++ {
		if s == arr[i] {
			return append(arr[:i], arr[i+1:]...)
		}
	}
	return arr
}

// 移除数组中的元素
//
//	s       待移除的变量
//	arr     待移除变量的数组
func RemoveInt16Array(s int16, arr []int16) []int16 {
	for i := 0; i < len(arr); i++ {
		if s == arr[i] {
			return append(arr[:i], arr[i+1:]...)
		}
	}
	return arr
}

// 移除数组中的元素
//
//	s       待移除的变量
//	arr     待移除变量的数组
func RemoveUintArray(s uint, arr []uint) []uint {
	for i := 0; i < len(arr); i++ {
		if s == arr[i] {
			return append(arr[:i], arr[i+1:]...)
		}
	}
	return arr
}

// 移除数组中的元素
//
//	s       待移除的变量
//	arr     待移除变量的数组
func RemoveIntArray(s int, arr []int) []int {
	for i := 0; i < len(arr); i++ {
		if s == arr[i] {
			return append(arr[:i], arr[i+1:]...)
		}
	}
	return arr
}

// 移除数组中的元素
//
//	s       待移除的变量
//	arr     待移除变量的数组
func RemoveUint32Array(s uint32, arr []uint32) []uint32 {
	for i := 0; i < len(arr); i++ {
		if s == arr[i] {
			return append(arr[:i], arr[i+1:]...)
		}
	}
	return arr
}

// 移除数组中的元素
//
//	s       待移除的变量
//	arr     待移除变量的数组
func RemoveInt32Array(s int32, arr []int32) []int32 {
	for i := 0; i < len(arr); i++ {
		if s == arr[i] {
			return append(arr[:i], arr[i+1:]...)
		}
	}
	return arr
}

// 移除数组中的元素
//
//	s       待移除的变量
//	arr     待移除变量的数组
func RemoveUint64Array(s uint64, arr []uint64) []uint64 {
	for i := 0; i < len(arr); i++ {
		if s == arr[i] {
			return append(arr[:i], arr[i+1:]...)
		}
	}
	return arr
}

// 移除数组中的元素
//
//	s       待移除的变量
//	arr     待移除变量的数组
func RemoveInt64Array(s int64, arr []int64) []int64 {
	for i := 0; i < len(arr); i++ {
		if s == arr[i] {
			return append(arr[:i], arr[i+1:]...)
		}
	}
	return arr
}

// 移除数组中的元素
//
//	s       待移除的变量
//	arr     待移除变量的数组
func RemoveByteArray(s byte, arr []byte) []byte {
	for i := 0; i < len(arr); i++ {
		if s == arr[i] {
			return append(arr[:i], arr[i+1:]...)
		}
	}
	return arr
}

// 移除数组中的元素
//
//	s       待移除的变量
//	arr     待移除变量的数组
func RemoveRuneArray(s rune, arr []rune) []rune {
	for i := 0; i < len(arr); i++ {
		if s == arr[i] {
			return append(arr[:i], arr[i+1:]...)
		}
	}
	return arr
}

// 移除数组中的元素
//
//	s       待移除的变量
//	arr     待移除变量的数组
func RemoveStringArray(s string, arr []string) []string {
	for i := 0; i < len(arr); i++ {
		if s == arr[i] {
			return append(arr[:i], arr[i+1:]...)
		}
	}
	return arr
}

// 移除数组中的元素
//
//	s       待移除的变量
//	arr     待移除变量的数组
func RemoveComplex128Array(s complex128, arr []complex128) []complex128 {
	for i := 0; i < len(arr); i++ {
		if s == arr[i] {
			return append(arr[:i], arr[i+1:]...)
		}
	}
	return arr
}

// 移除数组中的元素
//
//	s       待移除的变量
//	arr     待移除变量的数组
func RemoveComplex64Array(s complex64, arr []complex64) []complex64 {
	for i := 0; i < len(arr); i++ {
		if s == arr[i] {
			return append(arr[:i], arr[i+1:]...)
		}
	}
	return arr
}

// 移除数组中的元素
//
//	s       待移除的变量
//	arr     待移除变量的数组
func RemoveUintptrArray(s uintptr, arr []uintptr) []uintptr {
	for i := 0; i < len(arr); i++ {
		if s == arr[i] {
			return append(arr[:i], arr[i+1:]...)
		}
	}
	return arr
}

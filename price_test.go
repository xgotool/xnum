package xnum_test

import (
	"testing"

	"gitee.com/xgotool/xnum"
)

func TestDiscount(t *testing.T) {
	t.Log("100/100%:", xnum.Discount(100, 100))
	t.Log("100/80%:", xnum.Discount(100, 80))
	t.Log("100/99%:", xnum.Discount(100, 99))
	t.Log("9.9/100%:", xnum.Discount(9.9, 100))
	t.Log("9.9/80%:", xnum.Discount(9.9, 80))
	t.Log("9.9/99%:", xnum.Discount(9.9, 99))
}

// 计算金额部分（保留两位小数）
func TestPrice(t *testing.T) {
	t.Log(xnum.Price(0.95 * 1.23 / 100))
}

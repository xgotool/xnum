// 数值运算相关支持包
package xnum

// 浮点数比较相等时的最小值
//
// 因为浮点数的精度存在丢失问题，所以浮点数的相同不能比较绝对的相等，而应该是在某一范围内相同
//
// 所以此处的相同浮点数范围表示为 0.000,000,1
const FLOAT_EQUAL_MIN = 0.0000001

// 地球半径大小
// 用于计算两个经纬度间的距离
const EARTH_RADIUS = 6378.137

// 经纬度GPS坐标
type GPS struct {
	Latitude  float64 `json:"latitude" form:"latitude" gorm:"column:latitude;type:double;comment:纬度"`    // 纬度  国内数值较小的数字4~53.5
	Longitude float64 `json:"longitude" form:"longitude" gorm:"column:longitude;type:double;comment:经度"` // 经度  国内数值较大的数字73.5~135
}

// 错误记录模块
type Config struct {
	PanicLog func(msg string, err error) // 错误日志记录
}

var _default Config = Config{}

// 模块注册函数，用于注入错误处理逻辑
func Regedit(c *Config) {
	if c == nil {
		return
	}
	if c.PanicLog != nil {
		_default.PanicLog = c.PanicLog
	}
}

package xnum

// 金额字段取整（保留两位小数）（舍弃）
//
//	v	需要保留位数的金额
func Price(v float64) float64 {
	return Divide(v, 1)
}

// 金额字段取整（保留两位小数）（四舍五入）
//
//	v	需要保留位数的金额
func PriceHalf(v float64) float64 {
	return Price(v + 0.005)
}

package xnum

import "math"

// 计算金额的折扣费率
// 此处使用math.Floor进行向下取整的方式，即低于0.01的部分直接舍弃
//
//	price		待计算的金额
//	discount	费率,0~100
func Discount(price float64, discount uint8) float64 {
	if discount > 100 {
		discount = 100
	}
	// 取整方式
	// 向上取整 math.Ceil(x)
	// 向下取整 math.Floor(x)
	// 四舍五入取整 math.Floor(x + 0.5)
	return math.Floor(price*100*float64(discount)/100) / 100
}
